import ListAgents from './list_agents.vue';
import CreateAgent from './create_agent.vue';

export { ListAgents, CreateAgent };
